#!/bin/bash

#Aprovisionamiento de software

#Actualizo los paquetes disponibles de la VM
sudo apt-get update -y

# Muevo el archivo de configuración de firewall al lugar correspondiente
#if [ -f "/tmp/ufw" ]; then
#	sudo mv -f /tmp/ufw /etc/default/ufw
#fi

##Swap
##Genero una partición swap. Previene errores de falta de memoria
if [ ! -f "/swapdir/swapfile" ]; then
	sudo mkdir /swapdir
	cd /swapdir
	sudo dd if=/dev/zero of=/swapdir/swapfile bs=1024 count=2000000
	sudo mkswap -f  /swapdir/swapfile
	sudo chmod 600 /swapdir/swapfile
	sudo swapon swapfile
	echo "/swapdir/swapfile       none    swap    sw      0       0" | sudo tee -a /etc/fstab /etc/fstab
	sudo sysctl vm.swappiness=10
	echo vm.swappiness = 10 | sudo tee -a /etc/sysctl.conf
fi

# Configuración applicación
# ruta raíz del servidor web
#APACHE_ROOT="/var/www"
# ruta de la aplicación
#APP_PATH="$APACHE_ROOT/utn-devops-app/"

######## Instalacion de DOCKER ########
#
# Esta instalación de docker es para demostrar el aprovisionamiento
# complejo mediante Vagrant. La herramienta Vagrant por si misma permite
# un aprovisionamiento de container mediante el archivo Vagrantfile. A fines
# del ejemplo que se desea mostrar en esta unidad que es la instalación mediante paquetes del
# software Docker este ejemplo es suficiente, para un uso más avanzado de Vagrant
# se puede consultar la documentación oficial en https://www.vagrantup.com
#
if [ ! -x "$(command -v docker)" ]; then
	sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

	##Configuramos el repositorio
	curl -fsSL "https://download.docker.com/linux/debian/gpg" > /tmp/docker_gpg
	sudo apt-key add < /tmp/docker_gpg && sudo rm -f /tmp/docker_gpg
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

	#Actualizo los paquetes con los nuevos repositorios
	sudo apt-get update -y

	#Instalo docker desde el repositorio oficial
	sudo apt-get install -y docker-ce docker-compose

	#Lo configuro para que inicie en el arranque
	sudo systemctl enable docker
fi

cd /vagrant_data || exit

if [ ! -f "/owlCloud" ]; then
	sudo mkdir /owlCloud
fi

sudo cp docker-compose.yml /owlCloud/

sudo cp owlCloud /etc/init.d/

sudo chmod +x /etc/init.d/owlCloud

sudo cp owlCloud.service /etc/systemd/system/

sudo systemctl enable owlCloud.service

sudo systemctl start owlCloud
